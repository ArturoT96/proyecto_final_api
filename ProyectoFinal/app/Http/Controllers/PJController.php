<?php

namespace App\Http\Controllers;

use App\Models\Personaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\PJServices;

class PJController extends Controller
{
    
    protected $pjServices;

    public function __construct()
    {
        $this->pjServices = new PJServices();
    }

    public function addPJ(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'nivel' => 'required',
            'vida' => 'required',
            'clase_armadura' => 'required',
        ], [
            'nombre.required' => 'Es necesario rellenar todos los campos.',
            'nivel.required' => 'Es necesario rellenar todos los campos.',
            'vida.required' => 'Es necesario rellenar todos los campos.',
            'clase_armadura' => 'Es necesario rellenar todos los campos.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $respuesta=$this->pjServices->anyadirPJ($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function editPJ(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'nivel' => 'required',
            'vida' => 'required',
            'clase_armadura' => 'required',
        ], [
            'nombre.required' => 'Es necesario rellenar todos los campos.',
            'nivel.required' => 'Es necesario rellenar todos los campos.',
            'vida.required' => 'Es necesario rellenar todos los campos.',
            'clase_armadura' => 'Es necesario rellenar todos los campos.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $respuesta=$this->pjServices->editarPJ($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function getEstados(Request $request){
        $respuesta=$this->pjServices->traerEstados($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function traerDatos(Request $request){

        $respuesta=$this->pjServices->traerDatosPJ($request);

        if($respuesta['vacio']==true){
            return response()->json(['vacio' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function deletePJ(Request $request){

        $respuesta=$this->pjServices->eliminarPJ($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function curarPJ(Request $request){
        $respuesta=$this->pjServices->curar($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function addTemp(Request $request){

        $respuesta=$this->pjServices->addTemporales($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function saludyTemp(Request $request){
        $respuesta=$this->pjServices->vidayTemp($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    /*public function editEstado(Request $request){
        $respuesta=$this->pjServices->editarEstado($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }*/
    public function editEstados(Request $request){
        $respuesta=$this->pjServices->editarEstados($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
}