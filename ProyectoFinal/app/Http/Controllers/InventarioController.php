<?php

namespace App\Http\Controllers;

use App\Services\InventarioServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class InventarioController extends Controller
{
    
    protected $inventarioServices;

    public function __construct()
    {
        $this->inventarioServices = new InventarioServices();
    }

    public function datosMonedasPeso(Request $request){

        $respuesta=$this->inventarioServices->traerMonedasPeso($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    public function sumarMonedas(Request $request){

        $respuesta=$this->inventarioServices->sumar($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    public function restarMonedas(Request $request){

        $respuesta=$this->inventarioServices->restar($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    public function addObjeto(Request $request){

        $respuesta=$this->inventarioServices->add($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    public function editObjeto(Request $request){

        $respuesta=$this->inventarioServices->edit($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    public function removeObjeto(Request $request){

        $respuesta=$this->inventarioServices->remove($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function traerDatosObj(Request $request){

        $respuesta=$this->inventarioServices->getDatosObj($request);

        if($respuesta['vacio']==true){
            return response()->json(['vacio' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
}
