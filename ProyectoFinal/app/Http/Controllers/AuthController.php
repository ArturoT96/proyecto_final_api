<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    
    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('name', 'password'))) {
            return response()->json([
                'error' => 'El usuario o la contraseña no son correctos'
            ], 401);
        }

        $user = User::where('name', $request['name'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
                'access_token' => $token,
                'token_type' => 'Bearer',
        ]);
    }

    public function register(Request $request){

        /*$validatedData = $request->validate([
        'name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255|unique:users',
                        'password' => 'required|string|min:8',
        ]);*/

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ], [
            'name.required' => 'Es necesario rellenar todos los campos.',
            'name.unique' => 'Este nombre ya está en uso.',
            'name.max' => 'El nombre no puede superar los 255 caracteres',
            'email.required' => 'Es necesario rellenar todos los campos.',
            'email.max' => 'El email no puede superar los 255 caracteres',
            'email.unique' => 'Este email ya está en uso',
            'password.required' => 'Es necesario rellenar todos los campos.',
            'password.min' => 'La contraseña debe tener al menos 8 caracteres',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' =>  $validator->errors()->first()], 422);
        }

      

        $user = User::create([
                'name' => $request -> name,
                    'email' => $request -> email,
                    'password' => Hash::make($request -> password),
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
                    /*'access_token' => $token,
                    'token_type' => 'Bearer',*/
                    "msg" => "Usuario registrado correctamente"
        ]);
    }

    /*public function me(Request $request)
    {
        return $request->user();
    }*/
}