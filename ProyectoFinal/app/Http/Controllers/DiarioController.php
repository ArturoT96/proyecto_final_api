<?php

namespace App\Http\Controllers;

use App\Models\Notas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\DiarioServices;

class DiarioController extends Controller
{
    
    protected $diarioServices;

    public function __construct()
    {
        $this->diarioServices = new DiarioServices();
    }

    public function getDiario(Request $request){
        $respuesta=$this->diarioServices->getDatosDiario($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function addPestanya(Request $request){

        $validator = Validator::make($request->all(), [
            'nombrePestanya' => 'required',
        ], [
            'nombrePestanya.required' => 'Es necesario nombrar la pestaña',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $respuesta=$this->diarioServices->anyadirPestanya($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function editPestanya(Request $request){

        $validator = Validator::make($request->all(), [
            'oldName' => 'required',
            'newName' => 'required',
        ], [
            'oldName.required' => 'Es necesario nombrar la pestaña',
            'newName.required' => 'Es necesario nombrar la pestaña',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $respuesta=$this->diarioServices->editarPestanya($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function deleteTab(Request $request){

        $respuesta=$this->diarioServices->borrarTab($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function deleteNota(Request $request){

        $respuesta=$this->diarioServices->borrarNota($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function deletePagina(Request $request){

        $respuesta=$this->diarioServices->borrarPagina($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    
    public function saveNota(Request $request){

        $respuesta=$this->diarioServices->guardarNota($request);

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 200);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function searchWord(Request $request){

        $respuesta=$this->diarioServices->buscarPalabra($request);
        

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 477);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }

    public function createPestanya(Request $request){

        $respuesta=$this->diarioServices->crearPestanya($request);
        

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 477);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
    public function anyadirPagina(Request $request){

        $respuesta=$this->diarioServices->addPagina($request);
        

        if($respuesta['error']==true){
            return response()->json(['error' => $respuesta['msg']], 477);
        }else{
            return response()->json(['succesful' => $respuesta['msg']], 200);
        }
    }
}