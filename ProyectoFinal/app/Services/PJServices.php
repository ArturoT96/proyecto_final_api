<?php

namespace App\Services;

use App\Models\Notas;
use App\Models\Personaje;
use App\Models\User;
use Illuminate\Http\Request;

class PJServices
{	
	public function anyadirPJ(Request $request)
	{
		$nombre = $request -> nombre;
		$nivel = $request -> nivel;
		$clase = $request -> clase;
		$campanya = $request -> campanya;
		$vida = $request -> vida;
		$foto = $request -> foto;
		$clase_armadura = $request -> clase_armadura;
		$userName = $request -> userName;

        $userID = User::where('name', $userName) -> first() -> id;

        $pjID = null;

        $lastIdAntes = Personaje::orderBy('id', 'desc')->first() -> id;

        if($foto == null){
            $personaje = Personaje::create([
                "nombre" => $nombre,
                "nivel" => $nivel,
                "clase" => $clase,
                "campanya" => $campanya,
                "salud_actual" => $vida,
                "salud_max" => $vida,
                "clase_armadura" => $clase_armadura,
                "FK_id_usuarios" => $userID
            ]);
            $pjID = $personaje -> id;
        }else{
            $personaje = Personaje::create([
                "nombre" => $nombre,
                "foto" => $foto,
                "nivel" => $nivel,
                "clase" => $clase,
                "campanya" => $campanya,
                "salud_actual" => $vida,
                "salud_max" => $vida,
                "clase_armadura" => $clase_armadura,
                "FK_id_usuarios" => $userID
            ]);
            $pjID = $personaje -> id;
        }
        
        $lastIdDepues = Personaje::orderBy('id', 'desc')->first() -> id;

        if($lastIdAntes != $lastIdDepues){
			$error=false;
			$msg = $lastIdDepues;
		}else {
			$error=true;
			$msg="Hubo un problema al añadir el personaje";
		}
		return array(
			'error' => $error,
			'msg' => $msg
		);

    }

    public function editarPJ(Request $request){
        $nombre = $request -> nombre;
		$nivel = $request -> nivel;
		$clase = $request -> clase;
		$campanya = $request -> campanya;
		$vida = $request -> vida;
		$foto = $request -> foto;
		$clase_armadura = $request -> clase_armadura;
		$id = $request -> id;

        $personaje = Personaje::where('id', $id) -> update([
            "nombre" => $nombre,
            "nivel" => $nivel,
            "clase" => $clase,
            "campanya" => $campanya,
            "salud_max" => $vida,
            "salud_actual" => $vida,
            "foto" => $foto,
            "clase_armadura" => $clase_armadura
        ]);

        if($personaje){
            return array(
                "error" => false,
                "msg" => "Personaje actualizado correctamente"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Hubo un error al editar los datos del personaje"
            );
        }
    }

    /*public function traerEstados(Request $request){
        $pjID = $request -> id;

        $personajeEstados = Personaje::select('envenenado', 'cegado', 'paralizado', 'petrificado', 'tumbado', 'agarrado', 'encantado', 'ensordecido', 'asustado', 'incapacitado', 'invisible', 'restringido', 'aturdido', 'inconsciente') -> where('id', $pjID) -> get() -> toArray();

        foreach($personajeEstados[0] as $key => $value){
            if($value == 1){
                $value = true;
                $personajeEstados[0][$key] = $value;
            }else{
                $value = false;
                $personajeEstados[0][$key] = $value;
            }
        }

        $salud = Personaje::select('salud_actual', 'salud_temp') -> where('id', $pjID) -> get() -> toArray();

        $arraytodo = array_merge($personajeEstados[0], $salud[0]);


        if(!empty($personajeEstados) && !empty($salud)){
            return array(
                "error" => false,
                "msg" => $arraytodo
            );
        }

        return array(
            "error" => true, 
            "msg" => "Hubo un error al traer los estados"
        );      

    }*/
    public function traerEstados(Request $request){
        $pjID = $request -> id;

        $personajeEstados = Personaje::select('envenenado', 'cegado', 'paralizado', 'petrificado', 'tumbado', 'agarrado', 'encantado', 'ensordecido', 'asustado', 'incapacitado', 'invisible', 'restringido', 'aturdido', 'inconsciente') -> where('id', $pjID) -> get() -> toArray();

        foreach($personajeEstados[0] as $key => $value){
            if($value == 1){
                $value = true;
                $personajeEstados[0][$key] = $value;
            }else{
                $value = false;
                $personajeEstados[0][$key] = $value;
            }
        }

        $salud = Personaje::select('salud_actual', 'salud_max', 'salud_temp') -> where('id', $pjID) -> get() -> toArray();

        $arraytodo = array_merge($personajeEstados[0], $salud[0]);


        if(!empty($personajeEstados) && !empty($salud)){
            return array(
                "error" => false,
                "msg" => $arraytodo
            );
        }

        return array(
            "error" => true, 
            "msg" => "Hubo un error al traer los estados"
        );      

    }

    public function traerDatosPJ(Request $request){
        $userName = $request -> userName;
        $userID = User::where("name", $userName) -> first() -> id;

        $row = Personaje::where("FK_id_usuarios", $userID) -> get();

        $nombres = [];
        $clases = [];
        $niveles = [];
        $campanyas = [];
        $fotos = [];
        $vidas_max = [];
        $vidas_actuales = [];
        $tempHPs = [];
        $clases_armaduras = [];
        $ides = [];
        $envenenados = [];
        $cegados = [];
        $paralizados = [];
        $petrificados = [];
        $tumbados = [];
        $agarrados = [];
        $encantados = [];
        $ensordecidos = [];
        $asustados = [];
        $incapacitados = [];
        $invisibles = [];
        $restringidos = [];
        $aturdidos = [];
        $inconscientes = [];
        $exhaustos = [];
        $pesos_equipo = [];
        $pzs_oros = [];
        $pzs_plata = [];
        $pzs_cobre = [];
        $pzs_platino = [];
        $pzs_electrum = [];
    

        if(!empty($row)){
            $count = 0;
            foreach ($row as $k) { 
                $nombres[$count] = $k->nombre;
                $clases[$count] = $k->clase;
                $niveles[$count] = $k->nivel;
                $campanyas[$count] = $k->campanya;
                $fotos[$count] = $k->foto;
                $vidas_max[$count] = $k->salud_max;
                $vidas_actuales[$count] = $k->salud_actual;
                $tempHPs[$count] = $k->salud_temp;
                $clases_armaduras[$count] = $k->clase_armadura;
                $ides[$count] = $k->id;
                $envenenados[$count] = $k->envenenado;
                $cegados[$count] = $k->cegado;
                $paralizados[$count] = $k->paralizado;
                $petrificados[$count] = $k->petrificado;
                $tumbados[$count] = $k->tumbado;
                $agarrados[$count] = $k->agarrado;
                $encantados[$count] = $k->encantado;
                $ensordecidos[$count] = $k->ensordecido;
                $asustados[$count] = $k->asustado;
                $incapacitados[$count] = $k->incapacitado;
                $invisibles[$count] = $k->invisible;
                $restringidos[$count] = $k->restringido;
                $aturdidos[$count] = $k->aturdido;
                $inconscientes[$count] = $k->inconsciente;
                $exhaustos[$count] = $k->exhausto;
                $pesos_equipo[$count] = $k->peso_equipo;
                $pzs_oros[$count] = $k->p_oro;
                $pzs_plata[$count] = $k->p_plata;
                $pzs_cobre[$count] = $k -> p_cobre;
                $pzs_platino[$count] = $k->p_platino;
                $pzs_electrum[$count] = $k->p_electrum;
    
                $count ++;
            }

            return array(
                "vacio" => false,
                "msg" => array(
                    "nombres" => $nombres,
                    "clases" => $clases,
                    "niveles" => $niveles,
                    "campanyas" => $campanyas,
                    "fotos" => $fotos,
                    "vidas_max" => $vidas_max,
                    "vidas_actuales" => $vidas_actuales,
                    "tempHPs" => $tempHPs,
                    "clases_armaduras" => $clases_armaduras,
                    "ides" => $ides,
                    "envenenados" => $envenenados,
                    "cegados" => $cegados,
                    "paralizados" => $paralizados,
                    "petrificados" => $petrificados,
                    "tumbados" => $tumbados,
                    "agarrados" => $agarrados,
                    "encantados" => $encantados,
                    "ensordecidos" => $ensordecidos,
                    "asustados" => $asustados,
                    "incapacitados" => $incapacitados,
                    "invisibles" => $invisibles,
                    "restringidos" => $restringidos,
                    "aturdidos" => $aturdidos,
                    "inconscientes" => $inconscientes,
                    "exhaustos" => $exhaustos,
                    "pesos_equipo" => $pesos_equipo,
                    "pzs_oro" => $pzs_oros,
                    "pzs_plata" => $pzs_plata,
                    "pzs_cobre" => $pzs_cobre,
                    "pzs_platino" => $pzs_platino,
                    "pzs_electrum" => $pzs_electrum,
                )
            );
        }else {
            return array(
                "vacio" => true,
                "msg" => ""
            );
        }
    }

    public function eliminarPJ(Request $request){
        $pjID = $request -> id;

        $personaje = Personaje::where('id',$pjID) -> delete();

        if($personaje == true){
            return array(
                "error" => false,
                "msg" => "El personaje fue eliminado correctamente"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "No se pudo eliminar al personaje"
            );
        }
    }

    public function curar(Request $request){
        $pjID = $request -> id;
        $vida = $request -> vida;


        $personaje = Personaje::where('id', $pjID) -> update([
            "salud_actual" => $vida
        ]);


        if($personaje == true){
            return array(
                "error" => false,
                "msg" => "Salud actualizada"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Error al actualizar la salud"
            );
        }
    }

    public function addTemporales(Request $request){
        $pjID = $request -> id;
        $temp = $request -> temp;


        $personaje = Personaje::where('id', $pjID) -> update([
            "salud_temp" => $temp
        ]);


        if($personaje == true){
            return array(
                "error" => false,
                "msg" => "Salud actualizada"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Error al actualizar la salud"
            );
        }
    }

    public function vidayTemp(Request $request){
        $pjID = $request -> id;
        $vida = $request -> vida;
        $temp = $request -> temp;


        $personaje = Personaje::where('id', $pjID) -> update([
            "salud_actual" => $vida,
            "salud_temp" => $temp
        ]);


        if($personaje == true){
            return array(
                "error" => false,
                "msg" => "Salud actualizada"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Error al actualizar la salud"
            );
        }
    }

    /*public function editarEstado(Request $request){
        $pjID = $request -> id;
        $key = $request -> key;
        $estado = $request -> estado;

        if($estado == true){
            $estado = 1;
        }else{
            $estado = 0;
        }


        $personaje = Personaje::where('id', $pjID) -> update([
            $key => $estado,
        ]);


        if($personaje == true){
            return array(
                "error" => false,
                "msg" => "$key actualizado"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Error al actualizar el estado"
            );
        }
    }*/
    public function editarEstados(Request $request){
        $pjID = $request -> id;
        $estados = $request -> estados;
        $arr_estadosNuevos = [];
        $arr_estadosBD = [];

        foreach($estados as $value){
            if($value == true){
                $value = 1;
            }else{
                $value = 0;
            }
            array_push($arr_estadosNuevos, $value);
        }

      


        $personajeEstados = Personaje::select('envenenado', 'cegado', 'paralizado', 'petrificado', 'tumbado', 'agarrado', 'encantado', 'ensordecido', 'asustado', 'incapacitado', 'invisible', 'restringido', 'aturdido', 'inconsciente', 'exhausto') -> where('id', $pjID) -> get() -> toArray();


        foreach($personajeEstados[0] as $value){
            array_push($arr_estadosBD, $value);
        }

        if($arr_estadosNuevos == $arr_estadosBD){
            return array(
                "error" => false,
                "msg" => "No hay cambios en los estados"
            );
        }else {
            $personaje = Personaje::where('id', $pjID) -> update([
                "envenenado" => $estados["envenenado"],
                "cegado" => $estados["cegado"],
                "paralizado" => $estados["paralizado"],
                "petrificado" => $estados["petrificado"],
                "tumbado" => $estados["tumbado"],
                "agarrado" => $estados["agarrado"],
                "encantado" => $estados["encantado"],
                "ensordecido" => $estados["ensordecido"],
                "asustado" => $estados["asustado"],
                "incapacitado" => $estados["incapacitado"],
                "invisible" => $estados["invisible"],
                "restringido" => $estados["restringido"],
                "aturdido" => $estados["aturdido"],
                "inconsciente" => $estados["inconsciente"],
                "exhausto" => $estados["exhausto"],
            ]);

            if($personaje == true){
                return array(
                    "error" => false,
                    "msg" => "Estados actualizados"
                );
            }else {
                return array(
                    "error" => true,
                    "msg" => "Error al actualizar los estados"
                );
            }
        }

        
        

       


        
    }
}