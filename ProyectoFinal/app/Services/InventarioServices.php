<?php

namespace App\Services;

use App\Models\Personaje;
use App\Models\Objetos;
use Illuminate\Http\Request;


class InventarioServices
{	
	public function traerMonedasPeso(Request $request)
	{
		$id = $request -> id;

        $datos = Personaje::select("p_platino", "p_oro", "p_electrum", "p_plata", "p_cobre") -> where("id" , $id) -> get() -> toArray();

        $pesos = Objetos::select("peso") -> where("FK_id_personaje", $id) -> get() -> toArray();

        
        $total = 0;

        foreach($pesos as $key => $value){
            $total = $total + $value["peso"];
        }

        

        if(!empty($datos)){
            foreach($datos[0] as $key => $value){
                if($value == null){
                    $value = 0;
                    $datos[0][$key] = $value;
                }
            }

            $datos[0]["peso_equipo"] = $total;

            return array(
                "error" => false,
                "msg" => $datos[0]
            );
        }

        return array(
            "error" => true, 
            "msg" => "Hubo un error al traer los datos"
        );    
    }
	public function sumar(Request $request)
	{
		$id = $request -> id;
		$cantidad = $request -> cantidad;
		$moneda = $request -> moneda;

        switch ($moneda) {
            case "ppt":
                $moneda = "p_platino";
                break;
            case "po":
                $moneda = "p_oro";
                break;
            case "pe":
                $moneda = "p_electrum";
                break;
            case "pp":
                $moneda = "p_plata";
                break;
            case "pc":
                $moneda = "p_cobre";
                break;
        }

        $oldValue = Personaje::select($moneda) -> where("id", $id) -> get() -> toArray();

        $total = $oldValue[0][$moneda] + $cantidad;

        $operacion = Personaje::where("id", $id)-> update([
            $moneda => $total
        ]);

        if($operacion){
            return array(
                "error" => false,
                "msg" => "Moneda actualizada correctamente"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Hubo un error al actualizar las monedas"
            );
        }

    }

	public function restar(Request $request)
	{
		$id = $request -> id;
		$cantidad = $request -> cantidad;
		$moneda = $request -> moneda;

        switch ($moneda) {
            case "ppt":
                $moneda = "p_platino";
                break;
            case "po":
                $moneda = "p_oro";
                break;
            case "pe":
                $moneda = "p_electrum";
                break;
            case "pp":
                $moneda = "p_plata";
                break;
            case "pc":
                $moneda = "p_cobre";
                break;
        }

        $oldValue = Personaje::select($moneda) -> where("id", $id) -> get() -> toArray();

        $total = $oldValue[0][$moneda] - $cantidad;

        if($total < 0){
            $total = 0;
        }

        $operacion = Personaje::where("id", $id)-> update([
            $moneda => $total
        ]);

        if($operacion){
            return array(
                "error" => false,
                "msg" => "Moneda actualizada correctamente"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Hubo un error al actualizar las monedas"
            );
        }   
    }
	
	public function add(Request $request)
	{
		$idPj = $request -> idPj;
		$idPag = $request -> idPag;
		$nombre = $request -> nombre;
		$descripcion = $request -> descripcion;
		$valor = $request -> valor;
		$moneda = $request -> moneda;
		$peso = $request -> peso;

        $objeto = Objetos::create([
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "peso" => $peso,
            "cantidad" => $valor,
            "tipo" => $idPag,
            "tipoMoneda" => $moneda,
            "FK_id_personaje" => $idPj
        ]);


        if($objeto != null){
            $pjID = $objeto -> ID;
            return array(
                "error" => false,
                "msg" => $pjID
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Hubo un error al añadir el objeto"
            );
        }

   
    }
	public function edit(Request $request)
	{
		$idObj = $request -> idObj;
		$nombre = $request -> nombre;
		$descripcion = $request -> descripcion;
		$valor = $request -> valor;
		$moneda = $request -> moneda;
		$peso = $request -> peso;

        $objeto = Objetos::where('id', $idObj) -> update([
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "peso" => $peso,
            "cantidad" => $valor,
            "tipoMoneda" => $moneda,
        ]);

        if($objeto){
            return array(
                "error" => false,
                "msg" => "Objeto actualizado correctamente"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Hubo un error al editar los datos del objeto"
            );
        }
    }
	public function remove(Request $request)
	{
		$idObj = $request -> idObj;

        $objeto = Objetos::where('id',$idObj) -> delete();

        if($objeto == true){
            return array(
                "error" => false,
                "msg" => "El objeto fue eliminado correctamente"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "No se pudo eliminar el objeto"
            );
        }
   
    }

	public function getDatosObj(Request $request)
	{
		$idPj = $request -> idPj;
		$idPag = $request -> idPag;
        
        $ides = [];
        $nombres = [];
        $descripciones = [];
        $pesos = [];
        $valores = [];
        $tiposMonedas = [];

        $row = Objetos::where("FK_id_personaje", $idPj) -> where("tipo", $idPag) -> get();
    

        if(!empty($row)){
            $count = 0;
            foreach ($row as $k) { 
                $nombres[$count] = $k->nombre;
                $descripciones[$count] = $k->descripcion;
                $pesos[$count] = $k->peso;
                $ides[$count] = $k->id;
                $valores[$count] = $k->cantidad;
                $tiposMonedas[$count] = $k->tipoMoneda;
    
                $count ++;
            }
            return array(
                "vacio" => false,
                "msg" => array(
                    "nombres" => $nombres,
                    "ides" => $ides,
                    "descripciones" => $descripciones,
                    "pesos" => $pesos,
                    "valores" => $valores,
                    "tiposMonedas" => $tiposMonedas,
                )
            );
        }else {
            return array(
                "vacio" => true,
                "msg" => ""
            );
        }
   
    }
}