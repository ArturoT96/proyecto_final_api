<?php

namespace App\Services;


use App\Models\Notas;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class DiarioServices
{	
    public function getDatosDiario(Request $request){
        $PjID = $request -> id;

        $arrayPestanyas = [];
        $arrayNotas = [];
        $nombrePestanya = "";
        

        $pestanyas = Notas::where("FK_id_personajes", $PjID) -> get() -> groupBy("pestanya") ->toArray();

        if(!empty($pestanyas)){
            foreach ($pestanyas as $k) {

                $nombrePestanya = $k[0]["pestanya"];

                $notas = Notas::where("FK_id_personajes", $PjID) -> where("pestanya", $nombrePestanya) -> get() -> toArray();
        
                $count2 = 0;
               
                $arrayNotas = [];

                for($i=0; $i< count($notas); $i++){
                    
                    $arrayNotas[$i] = $notas[$i]["nota"];
                }
                
                $count2++;
    
                $arrayPestanyas[$nombrePestanya]["notas"] = $arrayNotas;
            }


            return array(
                "error" => false,
                "msg" => $arrayPestanyas
            );
        }

        return array(
            "error" => true, 
            "msg" => "Hubo un error al recuperar las notas"
        );



        
    }
    public function anyadirPestanya(Request $request){
        $PjID = $request -> id;
        $nombrePestanya = $request -> nombrePestanya;
        //$new_page = $request -> new_page;

        $nota = new Notas;

        /*if($new_page == "inicial"){
            $nota -> pestanya = $nombrePestanya;
            $nota -> index_pag = 0;
            $nota -> FK_id_personajes = $PjID;
        }else {
            $lastNota = Notas::where('pestanya', $nombrePestanya) -> where('FK_id_personajes', $PjID) -> max('index_pag'); 

            $lastNota++;
            $nota -> pestanya = $nombrePestanya;
            $nota -> index_pag = $lastNota;
            $nota -> FK_id_personajes = $PjID;
        }*/

        $nota -> pestanya = $nombrePestanya;
        $nota -> index_pag = 0;
        $nota -> FK_id_personajes = $PjID;
        
        $nota->save();
    
        if($nota){
            return array(
                "error" => false, 
                "msg" => "Pestaña añadida"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al crear la pestaña"
            );
        } 
    }
    public function editarPestanya(Request $request){
        $pjID = $request -> id;
        $oldName = $request -> oldName;
        $newName = $request -> newName;

        $notas = Notas::where('pestanya', $oldName) -> where('FK_id_personajes', $pjID) -> update(['pestanya' => $newName]);
    
        if($notas != null || $notas > 0){
            return array(
                "error" => false, 
                "msg" => "Pestaña editada"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al editar la pestaña"
            );
        } 
    }

    public function borrarTab(Request $request){
        $pjID = $request -> id;
        $nombre = $request -> nombre;
        
        $notas = Notas::where('FK_id_personajes', $pjID) -> where('pestanya', $nombre) -> delete();
    
        if($notas == true){
            return array(
                "error" => false, 
                "msg" => "Pestaña y su contenido eliminados"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al eliminar la pestaña y su contenido"
            );
        } 
    }

    public function borrarNota(Request $request){
        $pjID = $request -> id;
        $nombre = $request -> nombre;
        $currentPage = $request -> currentPage;
        
       $notas = Notas::where('FK_id_personajes', $pjID) -> where('pestanya', $nombre) -> where('index_pag', $currentPage) -> update(['nota' => null]);
    
        if($notas != null || $notas > 0){
            return array(
                "error" => false, 
                "msg" => "Nota borrada"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al eliminar el contenido de la página"
            );
        }  
    }

    public function borrarPagina(Request $request){
        $pjID = $request -> id;
        $nombre = $request -> nombre;
        $currentPage = $request -> currentPage;
        
        $notas = Notas::where('FK_id_personajes', $pjID) -> where('pestanya', $nombre) -> where('index_pag', $currentPage) -> delete();
    
        if($notas == true){
            return array(
                "error" => false, 
                "msg" => "Página eliminada"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al eliminar la página"
            );
        } 
    }

    public function guardarNota (Request $request){
        $pjID = $request -> id;
        $nombre = $request -> nombre;
        $currentPage = $request -> currentPage;
        $nota = $request -> nota;
        
        $notas = Notas::where('pestanya', $nombre) -> where('FK_id_personajes', $pjID) -> where('index_pag', $currentPage) -> update(['nota' => $nota]);

        if($notas != null || $notas > 0){
            return array(
                "error" => false, 
                "msg" => "Nota guardada"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al guardar la nota"
            );
        } 
        
    }

    public function buscarPalabra (Request $request){
        $pjID = $request -> id;
        $palabra = $request -> palabra;
        
        $notas = Notas::select('pestanya', 'index_pag') -> where('FK_id_personajes', $pjID) -> where('nota', 'LIKE', "%$palabra%") -> get() -> toArray();
        
        if($notas != null || !isset($notas)){
            return array(
                "error" => false, 
                "msg" => $notas
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "No hay resultados"
            );
        } 
        
    }

    public function crearPestanya (Request $request){
        $pjID = $request -> id;
        
        
        $nota = Notas::create([
            "pestanya" => "Lugares",
            "nota" => "",
            "FK_id_personajes" => $pjID,
            "index_pag" => 0
        ]);
        
        if($nota != null){
            return array(
                "error" => false,
                "msg" => "Pestaña creada"
            );
        }else {
            return array(
                "error" => true,
                "msg" => "Hubo un error al crear la pestaña"
            );
        }
        
    }

    public function addPagina (Request $request){
        $PjID = $request -> id;
        $nombrePestanya = $request -> nombrePestanya;

        
        $lastNota = Notas::where('pestanya', $nombrePestanya) -> where('FK_id_personajes', $PjID) -> max('index_pag'); 

        $nota = new Notas;

        $lastNota++;
        $nota -> pestanya = $nombrePestanya;
        $nota -> index_pag = $lastNota;
        $nota -> FK_id_personajes = $PjID;

        $nota->save();
    
        if($nota){
            return array(
                "error" => false, 
                "msg" => "Página añadida"
            );
        }else {
            return array(
                "error" => true, 
                "msg" => "Hubo un error al añadir la página"
            );
        } 
       
        
    }
}
