<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notas extends Model
{
    use HasFactory;

    protected $table = 'notas';
    protected $primaryKey = 'ID';
    protected $guarded = [];
    //protected $fillable = ['nombre', "foto", "nivel", "clase", "campanya", "salud_actual", "salud_max", "clase_armadura", "FK_id_usuarios" ];

    public $timestamps = false;
}
