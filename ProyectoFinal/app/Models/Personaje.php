<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Personaje extends Model
{
    use HasFactory;

    protected $table = 'personajes';
    protected $primaryKey = 'ID';
    protected $fillable = ['nombre', "foto", "nivel", "clase", "campanya", "salud_actual", "salud_max", "clase_armadura", "FK_id_usuarios" ];
    /*protected $casts = [
        'envenenado' => 'boolean',
        'cegado' => 'boolean',
        'paralizado' => 'boolean',
        'petrificado' => 'boolean',
        'tumbado' => 'boolean',
        'agarrado' => 'boolean',
        'encantado' => 'boolean',
        'ensordecido' => 'boolean',
        'asustado' => 'boolean',
        'incapacitado' => 'boolean',
        'invisible' => 'boolean',
        'restringido' => 'boolean',
        'aturdido' => 'boolean',
        'inconsciente' => 'boolean',
        'exhausto' => 'boolean',
    ];*/

    public $timestamps = false;
}
