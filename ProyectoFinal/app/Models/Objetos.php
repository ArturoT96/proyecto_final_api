<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Objetos extends Model
{
    use HasFactory;

    protected $table = 'objetos';
    protected $primaryKey = 'ID';
    protected $guarded = [];

    public $timestamps = false;
}
