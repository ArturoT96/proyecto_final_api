<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PJController;
use App\Http\Controllers\DiarioController;
use App\Http\Controllers\InventarioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::post('/addPJ', [PJController::class, 'addPJ']);
Route::post('/editPJ', [PJController::class, 'editPJ']);
Route::post('/traerDatos', [PJController::class, 'traerDatos']);
Route::post('/getEstados', [PJController::class, 'getEstados']);
Route::post('/deletePJ', [PJController::class, 'deletePJ']);
Route::post('/curarPJ', [PJController::class, 'curarPJ']);
Route::post('/addTemp', [PJController::class, 'addTemp']);
Route::post('/saludyTemp', [PJController::class, 'saludyTemp']);
//Route::post('/editEstado', [PJController::class, 'editEstado']);
Route::post('/editEstados', [PJController::class, 'editEstados']);

Route::post('/createPestanya', [DiarioController::class, 'createPestanya']);
Route::post('/getDiario', [DiarioController::class, 'getDiario']);
Route::post('/addPestanya', [DiarioController::class, 'addPestanya']);
Route::post('/editPestanya', [DiarioController::class, 'editPestanya']);
Route::post('/deleteTab', [DiarioController::class, 'deleteTab']);
Route::post('/deleteNota', [DiarioController::class, 'deleteNota']);
Route::post('/deletePagina', [DiarioController::class, 'deletePagina']);
Route::post('/saveNota', [DiarioController::class, 'saveNota']);
Route::post('/searchWord', [DiarioController::class, 'searchWord']);
Route::post('/anyadirPagina', [DiarioController::class, 'anyadirPagina']);

Route::post('/datosMonedasPeso', [InventarioController::class, 'datosMonedasPeso']);
Route::post('/sumarMonedas', [InventarioController::class, 'sumarMonedas']);
Route::post('/restarMonedas', [InventarioController::class, 'restarMonedas']);
Route::post('/addObjeto', [InventarioController::class, 'addObjeto']);
Route::post('/editObjeto', [InventarioController::class, 'editObjeto']);
Route::post('/removeObjeto', [InventarioController::class, 'removeObjeto']);
Route::post('/traerDatosObj', [InventarioController::class, 'traerDatosObj']);


//Route::post('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
